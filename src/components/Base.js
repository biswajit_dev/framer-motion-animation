import React from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import { buttonAnimation } from "../animations/button.animation";

const containerVarient = {
  init: {
    x: "100vw",
    opacity: 0,
  },
  final: {
    x: 0,
    opacity: 1,
    transition: {
      delay: 0.5,
      type: "spring",
      stiffness: 80,
    },
  },
};

const nextVarient = {
  init: {
    x: "-100vw",
  },
  final: {
    x: 0,
    transition: {
      type: "spring",
      stiffness: 120,
    },
  },
};

const Base = ({ addBase, pizza }) => {
  const bases = ["Classic", "Thin & Crispy", "Thick Crust"];

  return (
    <motion.div
      variants={containerVarient}
      initial="init"
      animate="final"
      className="base container"
    >
      <h3>Step 1: Choose Your Base</h3>
      <ul>
        {bases.map((base) => {
          let spanClass = pizza.base === base ? "active" : "";
          return (
            <motion.li
              whileHover={{ scale: 1.3, color: "#f8e112", originX: 0 }}
              transition={{ type: "spring", stiffness: 250 }}
              key={base}
              onClick={() => addBase(base)}
            >
              <span className={spanClass}>{base}</span>
            </motion.li>
          );
        })}
      </ul>

      {pizza.base && (
        <motion.div variants={nextVarient} className="next">
          <Link to="/toppings">
            <motion.button variants={buttonAnimation} whileHover="hover">
              Next
            </motion.button>
          </Link>
        </motion.div>
      )}
    </motion.div>
  );
};

export default Base;
