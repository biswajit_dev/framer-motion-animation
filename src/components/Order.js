import React from "react";
import { motion } from "framer-motion";

const contantVarient = {
  init: {
    x: "100vw",
    opacity: 0,
  },
  final: {
    x: 0,
    opacity: 1,
    transition: {
      delay: 0.5,
      type: "spring",
      stiffness: 80,
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
      staggerChildren: 0.8
    },
  },
};

const childVarient = {
  init: {
    opacity: 0,
  },
  final: {
    opacity: 1,
    transition: { duration: 2 },
  },
};

const Order = ({ pizza }) => {
  return (
    <motion.div
      variants={contantVarient}
      initial="init"
      animate="final"
      className="container order"
    >
      <h2>{"Thank you for your order :)"}</h2>
      <motion.p variants={childVarient}>
        You ordered a {pizza.base} pizza with:
      </motion.p>
      {pizza.toppings.map((topping) => (
        <motion.div variants={childVarient} key={topping}>
          {topping}
        </motion.div>
      ))}
    </motion.div>
  );
};

export default Order;
